Device Checklist
================

This Checklist is a supplement for all porters to be able to inform users in detail about their progress, as well as track their readiness for being added as a community or core device.

For either qualifying as a community or a core device a corresponding feature set from the list below needs to be confirmed working. The details will be published soon here.

For the convenience of your users present this list in the following way:

Working
-------

Working with additional steps
-----------------------------

Not working
-----------

Put all working features in under the first headline. Put features where the user needs to to additonal (manual) steps under the second headline. Everything else goes under the third headline.

* Actors: Manual brightness
* Actors: Notification LED
* Actors: Torchlight
* Actors: Vibration (works in QML apps, notifications, keyboard)
* Bluetooth: Driver loaded at startup
* Bluetooth: Enable/disable and flightmode works
* Bluetooth: Pairing with headset works, volume control ok
* Bluetooth: Persistent MAC address between reboots
* Camera: Ensure proper cameras are in use (On device with more than 2 logical cameras)
* Camera: Flashlight
* Camera: Photo
* Camera: Switch between back and front camera
* Camera: Video
* Cellular: Carrier info, signal strength
* Cellular: Change audio routings (Speakerphone, Earphone)
* Cellular: Data connection
* Cellular: Enable/disable mobile data and flightmode works
* Cellular: Incoming, outgoing calls
* Cellular: MMS in, out
* Cellular: PIN unlock
* Cellular: SMS in, out
* Cellular: Switch connection speed between 2G/3G/4G works for all SIMs
* Cellular: Switch preferred SIM for calling and SMS - only for devices that support it
* Cellular: Voice in calls
* Endurance: Battery lifetime > 24h from 100%
* Endurance: No reboot needed for 1 week
* GPU: Boot into SPinner animation and Lomiri UI
* GPU: Hardware video decoding
* Misc: Anbox patches applied to kernel
* Misc: AppArmor patches applied to kernel
* Misc: Battery percentage
* Misc: Date and time are correct after reboot (go to flight mode before)
* Misc: logcat, dmesg & syslog do not spam errors and service restarts
* Misc: Offline charging (Power down, connect USB cable, device should not boot to UT)
* Misc: Online charging (Green charging symbol, percentage increase in stats etc)
* Misc: Recovery image builds and works
* Misc: Reset to factory defaults
* Misc: SD card detection and access - only for devices that support it
* Misc: Shutdown / Reboot
* Misc: Wireless charging - only for devices that support it
* Network: NFC - only for devices that support it
* Sensors: Automatic brightness
* Sensors: Fingerprint reader, register and use fingerprints (Halium >=9.0 only)
* Sensors: GPS
* Sensors: Proximity works during a phone call
* Sensors: Rotation works in Lomiri
* Sensors: Touchscreen registers input across whole surface
* Sound: Earphones detected, volume control ok
* Sound: Loudspeaker, volume control ok
* Sound: Microphone, recording works
* Sound: System sounds and effects plays correctly (Camera shutter, Screenshot taken, Notifications)
* USB: ADB access
* USB: External monitor - only for devices that support it
* USB: MTP access
* WiFi: Driver loaded at startup
* WiFi: Enable/disable and flightmode works
* WiFi: Hotspot can be configured, switched on and off, can serve data to clients
* WiFi: Persistent MAC address between reboots

